﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FrontOffice.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //string apiUrl = "http://localhost:26404/api/";

            //WebClient client = new WebClient();
            //client.Headers["Content-type"] = "application/json";
            //client.Encoding = Encoding.UTF8;
            //string json = client.UploadString(apiUrl + "/values");
            ////List<CustomerModel> customers = (new JavaScriptSerializer()).Deserialize<List<CustomerModel>>(json);
            ////return customers;

            //var x = json;

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:53292/api/");
            //    //HTTP GET
            //    var responseTask = client.GetAsync("customer/");
            //    responseTask.Wait();

            //    var result = responseTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        var readTask = result.Content.ReadAsStringAsync();
            //        readTask.Wait();

            //        var test = readTask.Result;
            //        ViewBag.CustomerName = test;
            //    }
            //    else //web api sent error response 
            //    {
            //        //log response status here..

            //        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            //    }
            //}

            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}